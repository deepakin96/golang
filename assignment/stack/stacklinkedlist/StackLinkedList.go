package main

import (
	"fmt"
	"log"
	"time"
)

type myStack interface {
	pop()
	push(*node)
}
type node struct {
	data int
	next *node
}

type List struct {
	head *node
}

func main() {
	nodeList := new(List)

	var n, m int
	defer duration(track("Total time :")) //This is used to calculate the execution time
	fmt.Println("How many Producers and Consumers do you need?")
	fmt.Scanln(&n)
	fmt.Println("How many Products do you need?")
	fmt.Scanln(&m)
	for i := 1; i <= n; i++ {
		nodeList.producer(i)
		//time.Sleep(1 * time.Second)
		nodeList.consumer(i)
		time.Sleep(1 * time.Second)
	}

}
func (l *List) producer(id int) {
	m := id
	for i := 0; i <= m; i++ {
		info := &node{i, nil}
		l.push(info)
		fmt.Println("This is number", i, "--producer:", id)
	}
}
func (l *List) consumer(id int) {
	m := id
	for i := 0; i <= m; i++ {
		l.pop()
		fmt.Println("This is number", i, "--consumer:", id)
	}
}
func track(text string) (string, time.Time) {
	return text, time.Now()
}

func duration(text string, start time.Time) {
	log.Printf("%v: %v\n", text, time.Since(start))
}

func (nodeList *List) push(newNode *node) {
	if nodeList.head == nil {
		//fmt.Println(nodeList)
		nodeList.head = newNode
		//printList(nodeList)

	} else {
		n := nodeList.head
		for n.next != nil {

			n = n.next
		}
		fmt.Println("adding new node ")
		n.next = newNode
		fmt.Println(n.next)

	}

}

func (nodeList *List) pop() {
	if nodeList.head == nil {
		fmt.Printf("Cannot delete empty list")
		return
	}
	currentnode := nodeList.head
	if currentnode.next == nil {
		fmt.Print("the node deleted  ", currentnode.data)
		nodeList.head = nil

	} else {
		n := currentnode
		for n.next.next != nil {
			n = n.next
			fmt.Println("entered loop")
		}

		n.next = nil
		fmt.Print("deleted node at end \n ")

		//return nodeList
	}
}

//return nodeList

func (nodeList *List) printList() {
	if nodeList.head == nil {
		fmt.Printf("Cannot print empty list")
		return
	}
	n := nodeList.head
	for n.next != nil {
		fmt.Println(n)
		n = n.next
	}
	fmt.Println(n)
}
