package main

import (
	"fmt"
	"log"
	"sync"
	"time"
)

type myStack interface {
	pop()
	push(*node)
}
type node struct {
	data int
	next *node
}

type List struct {
	lock sync.Mutex
	head *node
}

func main() {
	nodeList := new(List)

	var n, m int
	defer duration(track("Total time :")) //This is used to calculate the execution time
	fmt.Println("How many Producers and Consumers do you need?")
	fmt.Scanln(&n)
	fmt.Println("How many Products do you need?")
	fmt.Scanln(&m)
	for i := 1; i <= n; i++ {
		nodeList.producer(i)
		//time.Sleep(1 * time.Second)
		nodeList.consumer(i)
		time.Sleep(1 * time.Second)
	}

}
func (l *List) producer(id int) {
	m := id
	for i := 0; i <= m; i++ {
		info := &node{i, nil}
		go l.push(info)
		fmt.Println("This is number", i, "--producer:", id)
	}
}
func (l *List) consumer(id int) {
	m := id
	for i := 0; i <= m; i++ {
		go l.pop()
		fmt.Println("This is number", i, "--consumer:", id)
	}
}
func track(text string) (string, time.Time) {
	return text, time.Now()
}

func duration(text string, start time.Time) {
	log.Printf("%v: %v\n", text, time.Since(start))
}

func (nodeList *List) push(newNode *node) {
	nodeList.lock.Lock()
	defer nodeList.lock.Unlock()
	if nodeList.head == nil {
		//fmt.Println(nodeList)
		nodeList.head = newNode
		//printList(nodeList)

	} else {
		n := nodeList.head
		for n.next != nil {

			n = n.next
		}

		n.next = newNode
		fmt.Println(n.next)

	}

}

func (nodeList *List) pop() {
	nodeList.lock.Lock()
	defer nodeList.lock.Unlock()
	if nodeList.head == nil {
		fmt.Printf("\nCannot delete empty list\n")
		return
	}
	currentnode := nodeList.head
	if currentnode.next == nil {
		fmt.Print("\nthe node deleted  ", currentnode.data)
		nodeList.head = nil

	} else {
		n := currentnode
		for n.next.next != nil {
			n = n.next
		}

		fmt.Print("deleted node at end \n ", n.next)
		n.next = nil
		//return nodeList
	}
}

//return nodeList

func (nodeList *List) printList() {
	nodeList.lock.Lock()
	defer nodeList.lock.Unlock()
	if nodeList.head == nil {
		fmt.Printf("\nCannot print empty list\n")
		return
	}
	n := nodeList.head
	for n.next != nil {
		fmt.Println(n)
		n = n.next
	}
	fmt.Println(n)
}
