package main

import (
	"errors"
	"fmt"
	"log"
	"math/rand"
	"sync"
	"time"
)

type myStack interface {
	pop() (int, error)
	push(int)
}
type stack struct {
	arr  [10]int
	size int
	lock sync.Mutex
}

func (s *stack) push(num int) {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.arr[s.size] = num
	s.size++
	fmt.Printf("pushed element %d into stack", num)

}
func (s *stack) pop() (int, error) {
	s.lock.Lock()
	defer s.lock.Unlock()
	if s.size == 0 {
		fmt.Print("Stack is empty")
		return 0, errors.New("Empty Stack")
	} else {
		i := s.arr[s.size-1]
		s.size--
		return i, nil

	}
}
func (s *stack) view() {
	s.lock.Lock()
	defer s.lock.Unlock()
	if s.size == 0 {
		fmt.Print("Stack is empty")
		return
	} else {
		for i := 0; i < s.size; i++ {
			fmt.Printf("stack element number %d is %d\n", i, s.arr[i])
		}
	}
}

func randomnumber() int {
	n := 0

	for i := 0; i < 1; i++ {
		n = rand.Intn(1000)
	}
	return n
}

func main() {
	s := new(stack)
	var n, m int
	defer duration(track("Total time :")) //This is used to calculate the execution time
	fmt.Println("How many Producers and Consumers do you need?")
	fmt.Scanln(&n)
	fmt.Println("How many Products do you need?")
	fmt.Scanln(&m)
	for i := 1; i <= n; i++ {
		s.producer(i)
		//time.Sleep(1 * time.Second)
		s.consumer(i)
		time.Sleep(1 * time.Second)
	}

}
func (s *stack) producer(id int) {
	m := id
	for i := 0; i <= m; i++ {
		go s.push(i)
		fmt.Println("This is number", i, "--producer:", id)
	}
}
func (s *stack) consumer(id int) {
	m := id
	for i := 0; i <= m; i++ {
		go s.pop()
		fmt.Println("This is number", i, "--consumer:", id)
	}
}
func track(text string) (string, time.Time) {
	return text, time.Now()
}

func duration(text string, start time.Time) {
	log.Printf("%v: %v\n", text, time.Since(start))
}
