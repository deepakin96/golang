package main

import (
	"fmt"
	"log"
	"time"
)

type tree struct {
	root    *Node
	element []int
}

type Node struct {
	data  int
	left  *Node
	right *Node
}

func (t *tree) push(num int) {
	t.element = append(t.element, num)
	if t.root == nil {
		t.root = &Node{data: num}
	} else {
		t.root.insert(num)
	}
}

func (n *Node) insert(num int) {
	if num <= n.data {
		if n.left == nil {
			n.left = &Node{data: num}
		} else {
			n.left.insert(num)
		}
	} else {
		if n.right == nil {
			n.right = &Node{data: num}
		} else {
			n.right.insert(num)
		}
	}
}

func (n *Node) minval() *Node {
	temp := n
	for temp.left != nil {
		temp = temp.left
	}
	//fmt.Printf("traveresed subtree and found min element %d \n", temp.data)
	return temp
}

func (n *Node) delete(num int) *Node {
	//fmt.Println("entereed deletion function")
	if n == nil {
		return n
	}
	if num < n.data {
		//fmt.Printf("the %d is lesser than node value of %d going to left subtree \n", num, n.data)
		n.left = n.left.delete(num)
		return n
	}
	if num > n.data {
		//fmt.Printf("the %d is greater than node value of %d going to right subtree \n", num, n.data)
		n.right = n.right.delete(num)
		return n
	} else {
		if n.left == nil {
			//fmt.Println("n.left empty condition")
			fmt.Printf("deleted element is %d \n", n.data)
			temp := n.right
			n = nil
			return temp

		}
		if n.right == nil {
			//fmt.Println("n.right empty condition")
			fmt.Printf("deleted element is %d \n", n.data)
			temp := n.left
			n = nil
			return temp
		}

		temp := n.right.minval()
		//fmt.Printf("replacing %d with %d obtained from minval function", n.data, temp.data)
		n.data = temp.data
		n.right = n.right.delete(temp.data)

	}

	return n

}

func (t *tree) pop() {
	if t.root == nil {
		fmt.Println("what are you doing trying to pop an empty tree ??")
		return
	}
	num := t.element[len(t.element)-1]
	t.element = t.element[:len(t.element)-1]
	t.root = t.root.delete(num)
}

func printPreOrder(n *Node) {
	if n == nil {
		return
	} else {
		fmt.Printf("%d ", n.data)
		printPreOrder(n.left)
		printPreOrder(n.right)
	}
}

func printPostOrder(n *Node) {
	if n == nil {
		return
	} else {
		printPostOrder(n.left)
		printPostOrder(n.right)
		fmt.Printf("%d ", n.data)
	}
}

func printInOrder(n *Node) {
	if n == nil {
		return
	} else {
		printInOrder(n.left)
		fmt.Printf("%d ", n.data)
		printInOrder(n.right)

	}
}
func main() {
	//first := new(Node)
	btree := new(tree)

	var n, m int
	defer duration(track("Total time :")) //This is used to calculate the execution time
	fmt.Println("How many Producers and Consumers do you need?")
	fmt.Scanln(&n)
	fmt.Println("How many Products do you need?")
	fmt.Scanln(&m)
	for i := 1; i <= n; i++ {
		btree.producer(i)
		//time.Sleep(1 * time.Second)
		btree.consumer(i)
		time.Sleep(1 * time.Second)
	}

}
func (t *tree) producer(id int) {
	m := id
	for i := 0; i <= m; i++ {
		t.push(i)
		fmt.Println("This is number", i, "--producer:", id)
	}
}
func (t *tree) consumer(id int) {
	m := id
	for i := 0; i <= m; i++ {
		t.pop()
		fmt.Println("This is number", i, "--consumer:", id)
	}
}
func track(text string) (string, time.Time) {
	return text, time.Now()
}

func duration(text string, start time.Time) {
	log.Printf("%v: %v\n", text, time.Since(start))
}
