package main

import (
	"fmt"
	"log"
	"sync"
	"time"
)

type cirqueue struct {
	lock   sync.Mutex
	cq_arr [5]int
	//MAX    int
}

var front int = -1
var rear int = -1

func (c *cirqueue) addelement(i int) {
	c.lock.Lock()
	defer c.lock.Unlock()
	fmt.Println(front)
	fmt.Println(rear)
	if front == 0 && rear == (len(c.cq_arr)-1) || rear+1 == front {
		fmt.Print("Queue Overflow \n")
		return
	}
	if rear == -1 { /*If queue is empty */

		front = 0
		rear = 0
	} else {
		if rear == (len(c.cq_arr) - 1) { /*rear is at last position of queue */
			rear = 0
		} else {
			rear = rear + 1
		}
	}
	c.cq_arr[rear] = i
}
func (c *cirqueue) del() {
	c.lock.Lock()
	defer c.lock.Unlock()
	if front == -1 {
		fmt.Print("Queue Underflow\n")
		return
	}
	fmt.Printf("Element deleted from queue is : %d\n", c.cq_arr[front])
	if front == rear { /* queue has only one element */
		front = -1
		rear = -1
	} else {
		if front == (len(c.cq_arr) - 1) {
			front = 0
		} else {
			front = front + 1
		}
	}
}
func (c *cirqueue) display() {

	front_pos := front
	rear_pos := rear
	if front == -1 {
		fmt.Print("Queue is empty\n")
		return
	}
	fmt.Print("Queue elements :\n")
	if front_pos <= rear_pos {
		for front_pos <= rear_pos {
			fmt.Printf("%d ", c.cq_arr[front_pos])
			front_pos++
		}
	} else {
		for front_pos <= (len(c.cq_arr) - 1) {
			fmt.Printf("%d ", c.cq_arr[front_pos])
			front_pos++
		}
		front_pos = 0
		for front_pos <= rear_pos {
			fmt.Printf("%d ", c.cq_arr[front_pos])
			front_pos++
		}
	}
	fmt.Print("\n")
}
func main() {
	c := new(cirqueue)

	var n, m int
	defer duration(track("Total time :")) //This is used to calculate the execution time
	fmt.Println("How many Producers and Consumers do you need?")
	fmt.Scanln(&n)
	fmt.Println("How many Products do you need?")
	fmt.Scanln(&m)
	for i := 1; i <= n; i++ {
		c.producer(i)
		//time.Sleep(1 * time.Second)
		c.consumer(i)
		time.Sleep(1 * time.Second)
	}

}
func (c *cirqueue) producer(id int) {
	m := id
	for i := 0; i <= m; i++ {
		go c.addelement(i)
		fmt.Println("This is number", i, "--producer:", id)
	}
}
func (c *cirqueue) consumer(id int) {
	m := id
	for i := 0; i <= m; i++ {
		go c.del()
		fmt.Println("This is number", i, "--consumer:", id)
	}
}
func track(text string) (string, time.Time) {
	return text, time.Now()
}

func duration(text string, start time.Time) {
	log.Printf("%v: %v\n", text, time.Since(start))
}
