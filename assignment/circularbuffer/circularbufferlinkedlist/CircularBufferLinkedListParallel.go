package main

import (
	"fmt"
	"log"
	"sync"
	"time"
)

type Node struct {
	data int
	link *Node
}

type Queue struct {
	lock sync.Mutex
	head *Node
	tail *Node
}

func (q *Queue) enQueue(value int) {
	q.lock.Lock()
	defer q.lock.Unlock()
	temp := new(Node)
	temp.data = value
	if q.head == nil {
		q.head = temp
	} else {
		q.tail.link = temp
	}

	q.tail = temp
	q.tail.link = q.head
}

func (q *Queue) deQueue() {
	q.lock.Lock()
	defer q.lock.Unlock()
	value := 0
	if q.head == nil {
		fmt.Print("Queue is empty")
		return
	}

	if q.head == q.tail {
		value = q.head.data

		q.head = nil
		q.tail = nil
	} else {
		temp := new(Node)
		temp = q.head
		value = temp.data
		q.head = q.head.link
		q.tail.link = q.head
		fmt.Printf("Element deleted from queue is : %d\n", value)
	}

	//return value
}

func (q *Queue) displayQueue() {
	temp := new(Node)
	temp = q.head
	if q.head == nil && q.tail == nil {
		fmt.Print("Queue is empty")
		return
	}
	fmt.Print("\nElements in Circular Queue are: ")
	for temp.link != q.head {
		fmt.Printf("%d ", temp.data)
		temp = temp.link
	}
	fmt.Printf("%d", temp.data)
}

func main() {
	c := new(Queue)

	var n, m int
	defer duration(track("Total time :")) //This is used to calculate the execution time
	fmt.Println("How many Producers and Consumers do you need?")
	fmt.Scanln(&n)
	fmt.Println("How many Products do you need?")
	fmt.Scanln(&m)
	for i := 1; i <= n; i++ {
		c.producer(i)
		//time.Sleep(1 * time.Second)
		c.consumer(i)
		time.Sleep(1 * time.Second)
	}

}
func (c *Queue) producer(id int) {
	m := id
	for i := 0; i <= m; i++ {
		go c.enQueue(i)
		fmt.Println("This is number", i, "--producer:", id)
	}
}
func (c *Queue) consumer(id int) {
	m := id
	for i := 0; i <= m; i++ {
		go c.deQueue()
		fmt.Println("This is number", i, "--consumer:", id)
	}
}
func track(text string) (string, time.Time) {
	return text, time.Now()
}

func duration(text string, start time.Time) {
	log.Printf("%v: %v\n", text, time.Since(start))
}
